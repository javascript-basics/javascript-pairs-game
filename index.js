import {PairsPresenter} from "./pairs-game/pairs_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new PairsPresenter();
        }
    }
}

Starter.start();
