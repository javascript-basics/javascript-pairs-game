import {Timer} from "./timer.js";
import {Card} from "./card.js";

function* randomIndexGenerator() {
    let availableValues = [...Array(CARDS_COUNT).keys()];

    while (availableValues.length) {
        const value = availableValues[Math.floor(Math.random() * availableValues.length)];
        availableValues = availableValues.filter(item => item !== value);
        yield value;
    }
}

const PAIRS_COUNT = 5;
const CARDS_COUNT = PAIRS_COUNT * 2;

const FIRST_PAIR_VALUE = 'first';
const SECOND_PAIR_VALUE = 'second';
const THIRD_PAIR_VALUE = 'third';
const FOURTH_PAIR_VALUE = 'fourth';
const FIFTH_PAIR_VALUE = 'fifth';

const DEFAULT_TITLE_VALUE = 'Click Any Card To Begin';
const WINNING_TITLE_VALUE = 'You win';

const TIMEOUT = 2000;

export class PairsService {
    #timer = new Timer();
    #timeout;

    #correctPairs = 0;

    #indexGenerator;
    #cards = [];
    #values = [FIRST_PAIR_VALUE, SECOND_PAIR_VALUE, THIRD_PAIR_VALUE, FOURTH_PAIR_VALUE, FIFTH_PAIR_VALUE];

    #previousCard;

    get cards() {
        return this.#cards;
    }

    get title() {
        if (this.#correctPairs === PAIRS_COUNT) {
            return `${WINNING_TITLE_VALUE}: ${this.#timerValueToString()}`;
        }

        if (this.#timer.started) {
            return this.#timerValueToString();
        }

        return DEFAULT_TITLE_VALUE;
    }

    get winningCondition() {
        return this.#correctPairs === PAIRS_COUNT
    }

    #timerValueToString() {
        const convertToTwoNumberOfDigits = value => value < 10 ? '0' + value : value;

        const seconds = convertToTwoNumberOfDigits(this.#timer.seconds);
        const milliSeconds = convertToTwoNumberOfDigits(this.#timer.milliSeconds);

        return `${seconds}:${milliSeconds}`;
    }

    setNewCards() {
        this.#correctPairs = 0;
        this.#initCards();
    }

    #initCards() {
        this.#indexGenerator = randomIndexGenerator();
        this.#cards = [];
        this.#previousCard = null;
        this.#values.forEach(this.#addPair.bind(this));
    };

    #addPair(value) {
        for (let i = 0; i < 2; i++) {
            const index = this.#indexGenerator.next().value;
            const card = new Card(index, value);
            this.#cards.push(card);
        }
    }

    checkCard(cardValue, index) {
        if (!this.#timer.started) {
            this.#timer.startTimer();
        }

        const setTimeoutFn = () => this.#timeout = setTimeout(this.#resetPreviousValue.bind(this), TIMEOUT);

        if (!this.#timeout) {
            setTimeoutFn();
        }

        const card = this.#cards.find(item => item.index === index && item.value === cardValue);
        const correctValue = this.#previousCard && this.#previousCard.index !== index &&
            this.#previousCard.value === cardValue;

        if (correctValue) {
            clearTimeout(this.#timeout);
            this.#correctPairs++;

            this.#cards.forEach(item => {
                if (item.value === cardValue) {
                    (item || new Card()).isCorrect = true;
                }
            });
        } else {
            clearTimeout(this.#timeout);
            this.#previousCard = card;
            setTimeoutFn();
        }

        if (this.#correctPairs === PAIRS_COUNT) {
            this.#timer.stopTimer();
            this.#cards.forEach(card => card.isCorrect = true);
        }
    }

    #resetPreviousValue() {
        this.#previousCard = null;
    }
}
