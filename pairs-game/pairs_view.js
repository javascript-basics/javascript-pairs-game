function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

function* elementGenerator(className) {
    const buttons = document.getElementsByClassName(className);
    for (let i = 0; i < buttons.length; i++) {
        yield buttons.item(i);
    }
}

const CARD_CLASS = 'flip-card';
const CARD_BACK_CLASS = 'flip-card-back';
const CARD_CORRECT_CLASS = 'flip-card-correct';
const CARD_ACTIVE_CLASS = 'flip-card-active';

const TIMER_ELEMENT_CLASS = 'header';
const RESET_BTN_CLASS = 'reset-btn'

const RESET_BTN_SHOW_CLASS = 'reset-btn-show';

const TIMEOUT = 2000;

export class PairsView {
    #timerElement = Object.create(HTMLElement.prototype, {});
    #resetBtn = Object.create(HTMLElement.prototype, {});

    get cards() {
        return elementGenerator(CARD_CLASS);
    }

    get resetBtn() {
        return this.#resetBtn;
    }

    constructor() {
        this.#timerElement = getElementByClassName(TIMER_ELEMENT_CLASS);
        this.#resetBtn = getElementByClassName(RESET_BTN_CLASS);
    }

    getClassNameValueFromCard(card) {
        const cardBack = (card || new HTMLElement()).getElementsByClassName(CARD_BACK_CLASS).item(0);
        const classNames = cardBack.classList.values();

        for (let className of classNames) {
            if (className !== CARD_BACK_CLASS) {
                return className;
            }
        }
    }

    updateCardValue(index, cardValue) {
        const card = document.getElementsByClassName(CARD_CLASS).item(index);
        const cardBack = card.getElementsByClassName(CARD_BACK_CLASS).item(0);
        cardBack.classList.add(cardValue);
    }

    hasCardActive(card) {
        return (card || new HTMLElement()).classList.contains(CARD_ACTIVE_CLASS);
    }

    addCardActive(card) {
        (card || new HTMLElement()).classList.add(CARD_ACTIVE_CLASS);
        setTimeout(this.removeCardActive.bind(this, card), TIMEOUT);
    }

    getCardIndex(card) {
        return [...this.cards].indexOf(card);
    }

    removeCardActive(card) {
        (card || new HTMLElement()).classList.remove(CARD_ACTIVE_CLASS);
    }

    addCardCorrectClass(index) {
        const card = document.getElementsByClassName(CARD_CLASS).item(index);
        (card || new HTMLElement()).classList.add(CARD_CORRECT_CLASS);
    }

    removeTabIndex(index) {
        const card = document.getElementsByClassName(CARD_CLASS).item(index);
        (card || new HTMLElement()).removeAttribute('tabindex');
    }

    updateTimerContent(value) {
        this.#timerElement.innerText = value;
    }

    hideTimer() {
        this.#timerElement.style.display = 'none';
    }

    addResetBtnShowClass() {
        this.#resetBtn.classList.add(RESET_BTN_SHOW_CLASS);
    }

    showTimer() {
        this.#timerElement.style.display = 'block';
    }

    removeResetBtnShowClass() {
        this.#resetBtn.classList.remove(RESET_BTN_SHOW_CLASS);
    }

    getCardBack(card) {
        return (card || new HTMLElement()).getElementsByClassName(CARD_BACK_CLASS).item(0);
    }

    removeAllClassesFromCardBack(cardBack) {
        (cardBack || new HTMLElement()).removeAttribute('class');
    }

    addCardBackClass(cardBack) {
        (cardBack || new HTMLElement()).classList.add(CARD_BACK_CLASS);
    }

    removeCardCorrectClass(card) {
        (card || new HTMLElement()).classList.remove(CARD_CORRECT_CLASS);
    }

    resetTabIndexes() {
        let index = 1;
        for (let card of this.cards) {
            card.setAttribute('tabindex', index.toString());
            index++;
        }
    }
}
