export class Card {
    #index;
    #value;

    #isCorrect;

    get index() {
        return this.#index;
    }

    get value() {
        return this.#value;
    }

    get isCorrect() {
        return this.#isCorrect;
    }

    set isCorrect(value) {
        this.#isCorrect = value;
    }

    constructor(index, pairNumber) {
        this.#index = index;
        this.#value = pairNumber;
    }
}
