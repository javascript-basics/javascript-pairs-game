const MAX_MS_VALUE = 99;
const TIMER_TIMEOUT = 1;

export class Timer {
    #milliSeconds;
    #seconds;
    #started;

    #timer;

    get milliSeconds() {
        return this.#milliSeconds;
    }

    get seconds() {
        return this.#seconds;
    }

    get started() {
        return this.#started;
    }

    startTimer() {
        this.#milliSeconds = 0;
        this.#seconds = 0;

        this.#started = true;
        this.#timer = setInterval(this.#calculateTime.bind(this), TIMER_TIMEOUT);
    }

    #calculateTime() {
        if (this.#milliSeconds === MAX_MS_VALUE) {
            this.#milliSeconds = 0;
            this.#seconds++;
        }

        this.#milliSeconds++;
    }

    stopTimer() {
        this.#started = false;
        clearInterval(this.#timer);
    }
}
