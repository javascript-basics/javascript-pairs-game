import {PairsView} from "./pairs_view.js";
import {PairsService} from "./pairs_service.js";

const TIMER_TIMEOUT = 1;
const RESET_TIMEOUT = 2500;

export class PairsPresenter {
    #pairsView = new PairsView();
    #pairsService;

    #timer;

    constructor() {
        this.#init();
    }

    #init() {
        this.#pairsService = new PairsService();
        this.#pairsService.setNewCards();

        this.#initListeners();
        this.#initTimer();
    }

    #initListeners() {
        (this.#pairsView.resetBtn || new HTMLElement()).addEventListener('click', this.#handleResetBtn.bind(this));

        const cards = this.#pairsView.cards;

        for (let card of cards) {
            (card || new HTMLElement()).addEventListener('click', this.#handleClick.bind(this))
        }
    }

    #handleResetBtn() {
        this.#pairsView.removeResetBtnShowClass();
        this.#pairsView.showTimer();

        const cards = this.#pairsView.cards;

        for (let card of cards) {
            this.#pairsView.removeCardActive(card);
            this.#pairsView.removeCardCorrectClass(card);

            const cardBack = this.#pairsView.getCardBack(card);

            this.#pairsView.removeAllClassesFromCardBack(cardBack);
            this.#pairsView.addCardBackClass(cardBack);
        }

        this.#pairsView.resetTabIndexes();
        this.#pairsService.setNewCards();

        this.#initTimer();
    }

    #initTimer() {
        this.#timer = setInterval(this.#updateView.bind(this), TIMER_TIMEOUT);
    }

    #updateView() {
        const cards = this.#pairsService.cards;

        for (let card of cards) {
            this.#pairsView.updateCardValue(card.index, card.value);

            if (card.isCorrect) {
                this.#pairsView.addCardCorrectClass(card.index);
                this.#pairsView.removeTabIndex(card.index);
            }
        }

        this.#pairsView.updateTimerContent(this.#pairsService.title);

        if (this.#pairsService.winningCondition) {
            clearInterval(this.#timer);
            setTimeout(this.#initResetState.bind(this), RESET_TIMEOUT);
        }
    }

    #initResetState() {
        this.#pairsView.hideTimer();
        this.#pairsView.addResetBtnShowClass();
    }

    #handleClick(event) {
        const card = (event || new PointerEvent('click')).currentTarget;
        const cardValue = this.#pairsView.getClassNameValueFromCard(card);

        if (this.#pairsView.hasCardActive(card)) {
            return;
        }

        this.#pairsView.addCardActive(card);
        const index = this.#pairsView.getCardIndex(card);

        this.#pairsService.checkCard(cardValue, index);
    }
}
